<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        if (App::environment() === 'production') {
            exit('I just stopped you getting fired. Love, Amo.');
        }

        Model::unguard();

        $this->call('UserSeeder');
        $this->call('OauthSeeder');
        $this->call('RoleSeeder');

    }

}
