<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert(array(
            array('name'=>'test','email'=>'test@test.com','password'=>Hash::make('password'), 'role_id'=>5)
        ));

        DB::table('users')->insert(array(
            array('name'=>'Admin','email'=>'admin@test.com','password'=>Hash::make('root'), 'role_id'=>1)
        ));


        $this->command->info('User table seeded!');
    }
}
