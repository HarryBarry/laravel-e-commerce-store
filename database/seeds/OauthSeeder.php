<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OauthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //delete oauth_clients records
        DB::table('oauth_clients')->delete();
        //insert some dummy records
        DB::table('oauth_clients')->insert(array(
            array('id'=>'GXvOWazQ3lA6YSaFji','secret'=>'GXvOWazQ3lA.6/YSaFji','name'=>'client 1')
        ));


        //delete oauth_grants records
        DB::table('oauth_grants')->delete();
        //insert some dummy records
        DB::table('oauth_grants')->insert(array(
            array('id'=>'password','created_at'=>'','updated_at'=>'')
        ));


        //oauth_client_grants table records
        DB::table('oauth_client_grants')->delete();
        //insert some dummy records
        DB::table('oauth_client_grants')->insert(array(
            array('client_id'=>'GXvOWazQ3lA6YSaFji','grant_id'=>'password','created_at'=>'','updated_at'=>'')

        ));

        //oauth_client_grants table records
        DB::table('oauth_access_tokens')->delete();
        //insert some dummy records
        DB::table('oauth_access_tokens')->insert(array(
            array('id'=>'Axu63lz8B35RD3M5AA3ZQmXHQjUeljEoLm1yRwCG','session_id'=>'1','expire_time'=>'1473669332','create_at'=>'2016-09-05 08:35:32','updated_at'=>'2016-09-05 08:35:32')

        ));
    }
}
