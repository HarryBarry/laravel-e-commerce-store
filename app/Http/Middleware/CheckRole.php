<?php namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use LucaDegasperi\OAuth2Server\Authorizer;


class CheckRole{

    public function __construct(Authorizer $authorizer)
    {
        $this->authorizer = $authorizer;
    }

    public function handle($request, Closure $next)
    {
        $roles = $this->getRequiredRoleForRoute($request->route());

        $owner = $this->getOwnerData($request);


        if (isset($owner)) {
            if ($owner->hasRole($roles) || !$roles) {
                return $next($request);
            }
            return response([
                'error' => 'insufficient_role',
                'error_description' => 'You not have permission to access this resource.'
            ], 401);
        } else {
            return response([
                'error' => 'invalid_request',
                'error_description' => 'The request is missing a required parameter, includes an invalid parameter value, includes a parameter more than once, or is otherwise malformed. Check the "access token" parameter.'
            ], 400);
        }
    }

    private function getRequiredRoleForRoute($route)
    {
        $actions = $route->getAction();
        return isset($actions['roles']) ? $actions['roles'] : null;
    }

    private function getOwnerData($request) {
        $access_token = ($request->header('Authorization'))
            ? str_replace('Bearer ', '', Request::header('Authorization'))
            : Request::input('access_token');

        if ($access_token) {
            $access_token_data = DB::table('oauth_access_tokens')->where('id', $access_token)->get();

            if (!empty($access_token_data)) {
                $session_id = $access_token_data[0]->session_id;

                $session_data = DB::table('oauth_sessions')->where('id', $session_id)->get();
                $owner_id = intval($session_data[0]->owner_id);

                $owner = User::find($owner_id);
            } else {
                return null;
            }
        } else {
            $owner = null;
        }

        return $owner;
    }
}