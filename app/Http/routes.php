<?php
// CORS
header('Access-Control-Allow-Origin: http://localhost:3000');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: Authorization, Content-Type');

Route::get('/', 'WelcomeController@index');

// ---=== Front area ===----




// ---=== Admin area ===----

    // Get Access Token by password grant_type
    Route::post('/api/v1/oauth/access_token', function() {
        return response()->json(Authorizer::issueAccessToken());
    });


    Route::group(['before' => 'oauth', 'prefix' => 'api/v1'], function () {
        // Admin area
        Route::group(['prefix' => 'users', 'middleware' => 'roles', 'roles' => ['administrator']], function() {
            Route::get('/', 'Admin\UsersController@getAllUsers');
            Route::post('/create', 'Admin\UsersController@createNewUser');
            Route::delete('/deleteById/{id}', 'Admin\UsersController@deleteUserById');
            Route::put('/updateById/{id}', 'Admin\UsersController@updateUserById');
        });

        // Admin and manager area
        Route::group([ 'prefix' => 'pages', 'middleware' => 'roles', 'roles' => ['administrator', 'manager'] ], function() {
            Route::post('/create', 'Admin\PagesController@createNewPage');
            Route::put('/updateById/{id}', 'Admin\PagesController@updatePageById');
            Route::delete('/deleteById/{id}', 'Admin\PagesController@deletePageById');

        });

        //All users area
        Route::group(['prefix' => 'users', 'middleware' => 'roles', 'roles' => ['administrator', 'manager', 'company manager','user']], function() {
            // Users Ctrl
            Route::get('/current', 'Admin\UsersController@getCurrentUser');
        });

        Route::group(['prefix' => 'pages', 'middleware' => 'roles', 'roles' => ['administrator', 'manager', 'company manager','user']], function() {

            // Pages Ctrl
            Route::get('/', 'Admin\PagesController@getAllPages');
            Route::get('/{slug}', 'Admin\PagesController@getPageBySlug');
        });


    });