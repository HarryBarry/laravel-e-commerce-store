<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use LucaDegasperi\OAuth2Server\Authorizer;
use App\User;


class UsersController extends Controller {

	public function __construct()
	{
		$this->middleware('oauth');
		$this->middleware('oauth-user');
	}

	public function getCurrentUser(Authorizer $authorizer) {
		$user_id = $authorizer->getResourceOwnerId();
		$user = User::find($user_id);
		return response()->json($user);
	}
	
	public function getAllUsers() {
		$users = User::all();
		return response()->json($users);
	}

	public function createNewUser(Request $request) {
        $rules = array(
            'name'       => 'required',
            'email'      => 'required|email|unique:users,email',
            'password'   => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return [
                'message' => 'validation_faild',
                'errors' => $validator->errors()
            ];
        } else {
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();

            return response()->json(['message' => 'User '.$user->name.' added.']);
        }

    }

    public function updateUserById ($id, Request $request) {
        $rules = array(
            'name'       => 'required',
            'email'      => 'required|email|unique:users,email',
            'password'   => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return [
                'message' => 'validation_faild',
                'errors' => $validator->errors()
            ];
        } else {
            $user = User::where('id', $id);
            if($user->count() == 0) {
                return response()->json(['message' => 'No page found', 'error' => '404'], 404);
            }

            if ($request->name)
                $user->name = $request->name;
            if ($request->email)
                $user->email = $request->email;
            if ($request->password)
                $user->password = bcrypt($request->password);
            $user->save();

            return response()->json(['message' => 'User '.$user->name.' updated.']);
        }
    }

    public function deleteUserById($id) {
        $user = User::where('id', $id);
        if($user->count() == 0) {
            return response()->json(['message' => 'No page found', 'error' => '404'], 404);
        }
        $user->delete();
        return response()->json(['message' => 'User '.$id.' deleted.']);
    }

}
