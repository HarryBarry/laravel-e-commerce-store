<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Validator;
use App\Page;
use Illuminate\Http\Request;


class PagesController extends Controller
{

    public function getPageBySlug($slug) {
        $page = Page::where('slug', $slug)->first();
        if($page->count() == 0) {
            return response()->json(['message' => 'No page found', 'error' => '404'], 404);
        } else {
            return response()->json($page);
        }
    }

    public function getAllPages() {
        $pages = Page::all();
        return response()->json($pages);
    }

    // Admin area
    public function createNewPage(Request $request) {
        $slug = str_slug($request->title, '-');

        $rules = array(
            'title' => 'required',
            'slug' => 'unique'
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return [
                'message' => 'validation_faild',
                'errors' => $validator->errors()
            ];
        } else {
            $page_data = Page::where('slug', $slug);

            $page = new Page;
            if ($page_data->count() >= 1) {
                return [
                    'message' => 'validation_faild',
                    'errors' => 'This name of article is allready exists!'
                ];
            } else {
                $page->slug =  $slug;
            }

            $page->title = ($request->title) ? $request->title : '';
            $page->description = ($request->description) ? $request->description : '';
            $page->tags = ($request->tags) ? $request->tags : '';
            $page->thumbnail_url = ($request->thumbnail_url) ? $request->thumbnail_url : '';
            $page->meta_title = ($request->meta_title) ? $request->meta_title : '';
            $page->meta_description = ($request->meta_description) ? $request->meta_description : '';
            $page->author = ($request->author) ? $request->author : '';
            $page->status = ($request->status) ? $request->status : '';
            $page->save();

            return response()->json($page);
        }
    }


    public function updatePageById($id, Request $request) {
        $rules = array(
            'title' => 'required',
            'slug' => 'unique'
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return [
                'message' => 'validation_faild',
                'errors' => $validator->errors()
            ];
        } else {
            $page = new Page;

            if ($request->title)
                $page->title = $request->title;
            if ($request->slug)
                $page->slug = $request->slug;
            if ($request->description)
                $page->description = $request->description;
            if ($request->tags)
                $page->tags = $request->tags;
            if ($request->thumbnail_url)
                $page->thumbnail_url = $request->thumbnail_url;
            if ($request->meta_title)
                $page->meta_title = $request->meta_title;
            if ($request->meta_description)
                $page->meta_description = $request->meta_description;
            if ($request->author)
                $page->author = $request->author;
            if ($request->status)
                $page->status = $request->status;
            $page->save();

            return response()->json(['message' => 'Page with id = '.$id.' updated.']);
        }
    }

    public function deletePageById($id) {
        $page = Page::where('id', $id);
        if($page->count() == 0) {
            return response()->json($page);
        }
        $page->delete();
        return response()->json(['message' => 'Page '.$id.' deleted!']);
    }
}
