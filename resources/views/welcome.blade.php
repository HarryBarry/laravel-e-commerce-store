<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Winter-Expert API</title>
		<style>
			body {
				font-family: 'courier new';
				margin: 0;
			}

			.content {
				border-bottom: 1px solid #ccc;
				padding: 0 20px;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="content">
				<h1>E-commerce shop API documentation</h1>
				<h2>Many thanks to 97 Blue Squirrels for support!</h2>
				<p>Th magic happens on <a href="#">e-commerce.com</a></p>
			</div>
		</div>
	</body>
</html>
